import React, { useState, useEffect } from 'react';

export const PublicationContext = React.createContext({
    products: [],
    filterList: (id) => { },
    addList: () => { }
})

export default props => {
    const [publicationList, setPublicationList] = useState([]);

    const filterDeletedList = publicationId => {
        setPublicationList(currentPublicationList => {
            const updatedList = currentPublicationList.filter(list => list.id !== publicationId);
            return updatedList;
        });
    }
    const addList = data => {
        setPublicationList(currentPublicationList => {
            const updatedList = [...currentPublicationList, data]
            return updatedList;
        });
    }
    useEffect(() => {
        fetch('http://localhost:3000/publications')
            .then(Response => Response.json())
            .then(responseData => {
                setPublicationList(responseData)
            });
    }, []);
    return <PublicationContext.Provider value={{ products: publicationList, filterList: filterDeletedList, addList: addList }}>
        {props.children}
    </PublicationContext.Provider>
}
