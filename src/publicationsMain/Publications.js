import React, { useState, useContext } from "react";
import { List, Avatar, Button, Form, Input } from 'antd';
import 'antd/dist/antd.css';
import { PublicationContext } from '../context/publication-context';
import './publications.css';


export const Publications = props => {
    const { TextArea } = Input;
    const publicationList = useContext(PublicationContext).products;
    const filterDeletedList = useContext(PublicationContext).filterList;
    const addList = useContext(PublicationContext).addList;
    const [enteredTitle, setEnteredTitle] = useState('');
    const [enteredDescription, setEnteredDescription] = useState('');

    const deleteList = id => {
        fetch(`http://localhost:3000/publications/${id}`, { method: 'DELETE' })
            .then(response => {
                filterDeletedList(id)
            });
    };

   const onReset = (title, description) => {
        setEnteredDescription("");
        setEnteredTitle("");
    };

    const submitList = (title, description) => {
        fetch(`http://localhost:3000/publications`, {
            method: 'POST', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title: title,
                description: description,
            })
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            addList(data)
        });
    }

    const list = (
        <div>
            <h2 className="heading">Publication List</h2>
            <List
                itemLayout="horizontal"
                dataSource={publicationList}
                renderItem={item => (
                    <List.Item
                        actions={[<a key="list-loadmore-edit"></a>, <a key="list-loadmore-more" onClick={() => deleteList(item.id)}>delete</a>]}
                    >
                        <List.Item.Meta
                            avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={<a>{item.title}</a>}
                            description={item.description}
                        />
                    </List.Item>
                )}
            />
        </div>
    );

    const addToList = (
        <Form layout='vertical'>
            <Form.Item label="Title">
                <Input placeholder="Enter a title" value={enteredTitle} onChange={event => { setEnteredTitle(event.target.value); }} />
            </Form.Item>
            <Form.Item label="Description">
                <TextArea placeholder="Enter a description" rows={4} value={enteredDescription} onChange={event => { setEnteredDescription(event.target.value); }} />
            </Form.Item>
            <Form.Item>
                <Button disabled={!enteredTitle} type="primary" onClick={() => submitList(enteredTitle, enteredDescription)}>Submit</Button>
                <Button className="margin" htmlType="button" onClick={() => onReset(enteredTitle, enteredDescription)}>
                    Reset
          </Button>
            </Form.Item>
        </Form>
    );

    return (
        <div className="main-container">
            <h2 className="heading">Add to publications</h2>
            {addToList}
            {list}
        </div>
    )
};
