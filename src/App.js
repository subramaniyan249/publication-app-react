import logo from './logo.svg';
import './App.css';
import { Fragment } from 'react';
import { Publications } from './publicationsMain/Publications';
import  PublicationProvider from './context/publication-context'

function App() {
  return (

    <PublicationProvider>
      <Publications></Publications>
    </PublicationProvider>
  );
}

export default App;
